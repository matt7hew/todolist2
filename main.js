let todoTools;
let newTodo;
let todoID = 0;
let todoToEdit;
let todoToDelete;
const todoInput = document.querySelector('.todo__add-input');
const newTodoInput = document.querySelector('.todo__newinput');
const todoUl = document.querySelector('.todo__ul');
const allLi = document.getElementsByTagName('li');
const todoInfo = document.querySelector('.todo__info');
const popup = document.querySelector('.popup');
const popupInfo = document.querySelector('.popup__info');
const editInput = document.querySelector('#edit-thing');
const addBtn = document.querySelector('.todo__addbtn');
const saveBtn = document.querySelector('.save');
const cancelBtn = document.querySelector('.cancel');
const completeBtn = document.querySelector('.done');

const addTodo = () => {
	if (todoInput.value !== '') {
		const newTodo = document.createElement('li');
		newTodo.innerHTML = `<span class="todo__newinput">${todoInput.value}</span>`;
		todoID++;
		newTodo.setAttribute('id', `todo-${todoID}`);
		createTodoElements(newTodo);
		todoUl.append(newTodo);
		todoInfo.style.display = 'none';
		todoInput.value = '';
	} else {
		todoInfo.style.display = 'block';
		todoInfo.textContent = 'Podaj treść notatki.';
	}
};

const checkClick = (e) => {
	if (e.target.closest('button').classList.contains('done')) {
		e.target.closest('li').classList.toggle('completed');
	} else if (e.target.closest('button').classList.contains('edit')) {
		editTodo(e);
	} else if (e.target.closest('button').classList.contains('delete')) {
		deleteTodo(e);
	}
};

const deleteTodo = (e) => {
	todoToDelete = document.getElementById(`${e.target.closest('li').id}`);
	todoToDelete.remove();
	popup.style.display = 'none';

	if (allLi.length === 0) {
		todoInfo.style.display = 'block';
		todoInfo.textContent = 'Brak zadań na liście.';
	}
};

const editTodo = (e) => {
	todoToEdit = document.getElementById(`${e.target.closest('li').id}`);
	popup.style.display = 'flex';
	editInput.value = todoToEdit.firstChild.textContent;
};

saveTodoPopup = () => {
	if (editInput.value !== '') {
		todoToEdit.firstChild.textContent = editInput.value;
		popup.style.display = 'none';
		editInput.value = '';
		popupInfo.textContent = '';
	} else {
		popupInfo.textContent = 'Podaj jakąś treść!';
	}
};

cancelTodoPopup = () => {
	popup.style.display = 'none';
	editInput.value = '';
	popupInfo.textContent = '';
};

const createTodoElements = (newTodo) => {
	const todoTools = document.createElement('div');
	todoTools.classList.add('todo__tools');
	newTodo.append(todoTools);

	const doneBtn = document.createElement('button');
	doneBtn.classList.add('done');
	doneBtn.innerHTML = '<i class="fa-solid fa-check"></i>';

	const editBtn = document.createElement('button');
	editBtn.classList.add('edit');
	editBtn.innerHTML = '<i class="fa-solid fa-pen-to-square"></i>';

	const deleteBtn = document.createElement('button');
	deleteBtn.classList.add('delete');
	deleteBtn.innerHTML = '<i class="fa-solid fa-trash"></i>';

	todoTools.append(doneBtn, editBtn, deleteBtn);
};

const keyCheck = (e) => {
	if (e.keyCode == 13) {
		addTodo();
	}
};

addBtn.addEventListener('click', addTodo);
todoUl.addEventListener('click', checkClick);
saveBtn.addEventListener('click', saveTodoPopup);
cancelBtn.addEventListener('click', cancelTodoPopup);
todoInput.addEventListener('keyup', keyCheck);
